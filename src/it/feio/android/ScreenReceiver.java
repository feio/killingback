package it.feio.android;

import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenReceiver extends BroadcastReceiver {

	public static int screenStatus;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			screenStatus = 0;
			Log.i("BackLightKeys", "SCHERMO OFF");
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			screenStatus = 1;
			Log.i("BackLightKeys", "SCHERMO ON");
		}

//		Intent serviceIntent = new Intent(context, BackLightKeysService.class);
//		serviceIntent.putExtra("screen_state", screenStatus);
//		context.startService(serviceIntent);
		
		Runtime r = Runtime.getRuntime();
		try {
			r.exec("echo " + screenStatus + " > /sys/class/leds/keyboard-backlight/brightness");
		} catch (IOException e) {
			Log.e(Constants.TAG, e.getMessage());
		}
	}

}
