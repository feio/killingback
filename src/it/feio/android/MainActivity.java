package it.feio.android;

import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	private static ActivityManager _activityManager;
	Intent service;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		service = null;
		Button startBtn = (Button) findViewById(R.id.start);
		startBtn.setOnClickListener(this);
		Button stopBtn = (Button) findViewById(R.id.stop);
		stopBtn.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public static ActivityManager.RunningAppProcessInfo getForegroundApp(
			Context context) {

		ActivityManager.RunningAppProcessInfo result = null, info;

		if (_activityManager == null)
			_activityManager = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);

		List<ActivityManager.RunningAppProcessInfo> list = _activityManager
				.getRunningAppProcesses();
		Iterator<ActivityManager.RunningAppProcessInfo> i = list.iterator();

		while (i.hasNext()) {
			info = i.next();
			if (info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
					&& !isRunningService(info.processName, context)) {
				result = info;
				break;
			}
		}
		return result;
	}

	private static boolean isRunningService(String processname, Context context) {
		if (processname == null || processname.isEmpty())
			return false;

		ActivityManager.RunningServiceInfo service;

		if (_activityManager == null)
			_activityManager = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> l = _activityManager
				.getRunningServices(9999);
		Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
		while (i.hasNext()) {
			service = i.next();
			if (service.process.equals(processname))
				return true;
		}

		return false;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.start:
			startService();
			break;
		case R.id.stop:
			stopService();
			break;

		default:
			break;
		}

	}

	private void startService() {
		if (service == null) {
			Log.i(Constants.TAG, "starting service");
			service = new Intent(this, KillingService.class);
			startService(service);
		} else {
			Log.w(Constants.TAG, "service already running");
		}
	}

	private void stopService() {
		if (service != null) {
			Log.i(Constants.TAG, "stopping service");
			stopService(service);
		} else {
			Log.w(Constants.TAG, "service not running");
		}
	}

}
