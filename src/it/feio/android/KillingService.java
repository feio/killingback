package it.feio.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class KillingService extends Service {
	private NotificationManager mNM;

	protected void onHandleIntent(Intent intent) {
		try {
			Thread.sleep(5000);
			Log.i(Constants.TAG, "Sono vivo!");
		} catch (InterruptedException e) {
			Log.e(Constants.TAG, "Thread interrotto");
		}

	}

	@Override
	public void onCreate() {
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		// Display a notification about us starting. We put an icon in the
		// status bar.
		showNotification();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		while (true) {
			try {
				Thread.sleep(5000);
				Log.i(Constants.TAG, "Sono vivo!");
			} catch (InterruptedException e) {
				Log.e(Constants.TAG, "Thread interrotto");
			}
			return super.onStartCommand(intent, flags, startId);
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	/**
	 * Show a notification while this service is running.
	 */
	private void showNotification() {

		// Set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.ic_launcher,
				"Service started", System.currentTimeMillis());

		// The PendingIntent to launch our activity if the user selects this
		// notification
		// PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
		// new Intent(this, LocalServiceActivities.Controller.class), 0);
		PendingIntent contentIntent = null;

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, "Servizio avviato",
				"Service started", contentIntent);

		// Send the notification.
		mNM.notify(1, notification);
	}

}
